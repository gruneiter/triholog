const path = require('path');
import webpack from 'webpack';

module.exports = {
  mode: 'development',
  entry: {
    main:'./src/scripts/script.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: 'script.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [
          path.resolve(__dirname, 'node_modules'),
          path.resolve(__dirname, 'scr/scripts/external'),
        ],
        loader: "eslint-loader",
      },
    ],
  }
}
