export default function showHide () {
  let showHideBlocks = document.querySelectorAll('.show-hide');
  let showHideArr = Array.from(showHideBlocks);
  for (let item of showHideArr) {
    let itemTitle = item.firstElementChild;
    itemTitle.addEventListener('click', (e) => {
      e.currentTarget.nextElementSibling.classList.toggle('show-hide__body--active');
    })
  }
}
