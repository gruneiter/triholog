let mobileMenu = () => {
  let mobileSwitchers = document.querySelectorAll('.mobile-nav__parent-label');

  function mobileSwitch() {
    this.classList.toggle('mobile-nav__parent-label--active');
    this.nextElementSibling.classList.toggle('mobile-nav__sublist--display');
  }

  for ( let i = 0; i < mobileSwitchers.length; i++ ) {
    mobileSwitchers[i].addEventListener( 'click', mobileSwitch );
  }
};

export default mobileMenu;
