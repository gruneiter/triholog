export default function consultationSwitch() {
  const consSwitch = document.querySelectorAll('.consultation-aside__switch');

  for ( let i = 0; i < consSwitch.length; i++ ) {
    let switcher = consSwitch[i];
    switcher.addEventListener( 'click', () => {
      switcher.classList.toggle('consultation-aside__switch--active');
    } );
  }
}
