import flexslider from 'flexslider';
import fancybox from '@fancyapps/fancybox';
import consultationSwitch from '../blocks/consultation/consultation-aside/consultation-aside.js';
import mobileFilterMobile from '../blocks/catalog/catalog-filter-mobile/catalog-filter-mobile.js';
import mobileMenu from '../blocks/header/mobile-menu/mobile-menu.js';
import showHide from '../blocks/show-hide/show-hide.js';
import countdown from './external/jquery.countdown.js';
consultationSwitch();
mobileFilterMobile();
mobileMenu();
showHide();

$(document).ready(function() {
  $('.js-slider').flexslider({
    animation: "slide",
    selector: ".js-slider__list > .js-slider__item",
    controlNav: false,
    touch: true,
    prevText: "",
    nextText: "",
    slideshow: false
  });
  $.maskfn.definitions['~']='[+-]';
  $('body').delegate('input[type="tel"], input.phone', 'focus', function(){
      $(this).maskfn('+7 (999) 999-99-99');
  });
  $('.modal').fancybox({
    
  });
});
